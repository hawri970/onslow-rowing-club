-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 23, 2014 at 05:06 am
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onslow_rowing_club`
--

-- --------------------------------------------------------

--
-- Table structure for table `onslow_commentmeta`
--

CREATE TABLE IF NOT EXISTS `onslow_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `onslow_comments`
--

CREATE TABLE IF NOT EXISTS `onslow_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `onslow_comments`
--

INSERT INTO `onslow_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Mr WordPress', '', 'https://wordpress.org/', '', '2014-09-22 00:26:10', '2014-09-22 00:26:10', 'Hi, this is a comment.\nTo delete a comment, just log in and view the post&#039;s comments. There you will have the option to edit or delete them.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `onslow_links`
--

CREATE TABLE IF NOT EXISTS `onslow_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `onslow_options`
--

CREATE TABLE IF NOT EXISTS `onslow_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=159 ;

--
-- Dumping data for table `onslow_options`
--

INSERT INTO `onslow_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/~katherine.griffiths/wordpress', 'yes'),
(2, 'home', 'http://localhost/~katherine.griffiths/wordpress', 'yes'),
(3, 'blogname', 'Onslow College Rowing', 'yes'),
(4, 'blogdescription', 'Onslow College Rowing', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'kategriffithsphotography@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:43:"the-events-calendar/the-events-calendar.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '0', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', '', 'no'),
(41, 'template', 'origin', 'yes'),
(42, 'stylesheet', 'origin', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '1', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '29630', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '0', 'yes'),
(53, 'default_link_category', '0', 'yes'),
(54, 'show_on_front', 'posts', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(82, 'uninstall_plugins', 'a:0:{}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '0', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '29630', 'yes'),
(89, 'onslow_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:101:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:16:"edit_tribe_event";b:1;s:16:"read_tribe_event";b:1;s:18:"delete_tribe_event";b:1;s:19:"delete_tribe_events";b:1;s:17:"edit_tribe_events";b:1;s:24:"edit_others_tribe_events";b:1;s:26:"delete_others_tribe_events";b:1;s:20:"publish_tribe_events";b:1;s:27:"edit_published_tribe_events";b:1;s:29:"delete_published_tribe_events";b:1;s:27:"delete_private_tribe_events";b:1;s:25:"edit_private_tribe_events";b:1;s:25:"read_private_tribe_events";b:1;s:16:"edit_tribe_venue";b:1;s:16:"read_tribe_venue";b:1;s:18:"delete_tribe_venue";b:1;s:19:"delete_tribe_venues";b:1;s:17:"edit_tribe_venues";b:1;s:24:"edit_others_tribe_venues";b:1;s:26:"delete_others_tribe_venues";b:1;s:20:"publish_tribe_venues";b:1;s:27:"edit_published_tribe_venues";b:1;s:29:"delete_published_tribe_venues";b:1;s:27:"delete_private_tribe_venues";b:1;s:25:"edit_private_tribe_venues";b:1;s:25:"read_private_tribe_venues";b:1;s:20:"edit_tribe_organizer";b:1;s:20:"read_tribe_organizer";b:1;s:22:"delete_tribe_organizer";b:1;s:23:"delete_tribe_organizers";b:1;s:21:"edit_tribe_organizers";b:1;s:28:"edit_others_tribe_organizers";b:1;s:30:"delete_others_tribe_organizers";b:1;s:24:"publish_tribe_organizers";b:1;s:31:"edit_published_tribe_organizers";b:1;s:33:"delete_published_tribe_organizers";b:1;s:31:"delete_private_tribe_organizers";b:1;s:29:"edit_private_tribe_organizers";b:1;s:29:"read_private_tribe_organizers";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:73:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:16:"edit_tribe_event";b:1;s:16:"read_tribe_event";b:1;s:18:"delete_tribe_event";b:1;s:19:"delete_tribe_events";b:1;s:17:"edit_tribe_events";b:1;s:24:"edit_others_tribe_events";b:1;s:26:"delete_others_tribe_events";b:1;s:20:"publish_tribe_events";b:1;s:27:"edit_published_tribe_events";b:1;s:29:"delete_published_tribe_events";b:1;s:27:"delete_private_tribe_events";b:1;s:25:"edit_private_tribe_events";b:1;s:25:"read_private_tribe_events";b:1;s:16:"edit_tribe_venue";b:1;s:16:"read_tribe_venue";b:1;s:18:"delete_tribe_venue";b:1;s:19:"delete_tribe_venues";b:1;s:17:"edit_tribe_venues";b:1;s:24:"edit_others_tribe_venues";b:1;s:26:"delete_others_tribe_venues";b:1;s:20:"publish_tribe_venues";b:1;s:27:"edit_published_tribe_venues";b:1;s:29:"delete_published_tribe_venues";b:1;s:27:"delete_private_tribe_venues";b:1;s:25:"edit_private_tribe_venues";b:1;s:25:"read_private_tribe_venues";b:1;s:20:"edit_tribe_organizer";b:1;s:20:"read_tribe_organizer";b:1;s:22:"delete_tribe_organizer";b:1;s:23:"delete_tribe_organizers";b:1;s:21:"edit_tribe_organizers";b:1;s:28:"edit_others_tribe_organizers";b:1;s:30:"delete_others_tribe_organizers";b:1;s:24:"publish_tribe_organizers";b:1;s:31:"edit_published_tribe_organizers";b:1;s:33:"delete_published_tribe_organizers";b:1;s:31:"delete_private_tribe_organizers";b:1;s:29:"edit_private_tribe_organizers";b:1;s:29:"read_private_tribe_organizers";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:34:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;s:16:"edit_tribe_event";b:1;s:16:"read_tribe_event";b:1;s:18:"delete_tribe_event";b:1;s:19:"delete_tribe_events";b:1;s:17:"edit_tribe_events";b:1;s:20:"publish_tribe_events";b:1;s:27:"edit_published_tribe_events";b:1;s:29:"delete_published_tribe_events";b:1;s:16:"edit_tribe_venue";b:1;s:16:"read_tribe_venue";b:1;s:18:"delete_tribe_venue";b:1;s:19:"delete_tribe_venues";b:1;s:17:"edit_tribe_venues";b:1;s:20:"publish_tribe_venues";b:1;s:27:"edit_published_tribe_venues";b:1;s:29:"delete_published_tribe_venues";b:1;s:20:"edit_tribe_organizer";b:1;s:20:"read_tribe_organizer";b:1;s:22:"delete_tribe_organizer";b:1;s:23:"delete_tribe_organizers";b:1;s:21:"edit_tribe_organizers";b:1;s:24:"publish_tribe_organizers";b:1;s:31:"edit_published_tribe_organizers";b:1;s:33:"delete_published_tribe_organizers";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:20:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:16:"edit_tribe_event";b:1;s:16:"read_tribe_event";b:1;s:18:"delete_tribe_event";b:1;s:19:"delete_tribe_events";b:1;s:17:"edit_tribe_events";b:1;s:16:"edit_tribe_venue";b:1;s:16:"read_tribe_venue";b:1;s:18:"delete_tribe_venue";b:1;s:19:"delete_tribe_venues";b:1;s:17:"edit_tribe_venues";b:1;s:20:"edit_tribe_organizer";b:1;s:20:"read_tribe_organizer";b:1;s:22:"delete_tribe_organizer";b:1;s:23:"delete_tribe_organizers";b:1;s:21:"edit_tribe_organizers";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:5:{s:4:"read";b:1;s:7:"level_0";b:1;s:16:"read_tribe_event";b:1;s:20:"read_tribe_organizer";b:1;s:16:"read_tribe_venue";b:1;}}}', 'yes'),
(90, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(91, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'sidebars_widgets', 'a:5:{s:19:"wp_inactive_widgets";a:0:{}s:7:"primary";a:3:{i:0;s:14:"recent-posts-2";i:1;s:17:"recent-comments-2";i:2;s:6:"meta-2";}s:10:"subsidiary";a:0:{}s:14:"after-singular";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(96, 'cron', 'a:5:{i:1411442356;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1411457460;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1411475173;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1411520302;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(98, '_site_transient_update_core', 'O:8:"stdClass":3:{s:7:"updates";a:0:{}s:15:"version_checked";s:3:"4.0";s:12:"last_checked";i:1411432022;}', 'yes'),
(99, '_transient_random_seed', 'e837c829c79e35c88c8fa50db3cb6e64', 'yes'),
(100, '_site_transient_update_plugins', 'O:8:"stdClass":1:{s:12:"last_checked";i:1411432080;}', 'yes'),
(103, '_site_transient_update_themes', 'O:8:"stdClass":1:{s:12:"last_checked";i:1411434504;}', 'yes'),
(104, 'can_compress_scripts', '1', 'yes'),
(105, '_transient_timeout_plugin_slugs', '1411442240', 'no'),
(106, '_transient_plugin_slugs', 'a:3:{i:0;s:19:"akismet/akismet.php";i:1;s:9:"hello.php";i:2;s:43:"the-events-calendar/the-events-calendar.php";}', 'no'),
(112, 'theme_mods_twentyfourteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1411347519;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(113, 'current_theme', 'Origin', 'yes'),
(114, 'theme_mods_origin', 'a:13:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:7:"primary";i:2;}s:16:"background_color";s:0:"";s:12:"header_image";s:131:"http://localhost/~katherine.griffiths/wordpress/wp-content/uploads/2014/09/cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg";s:17:"header_image_data";O:8:"stdClass":5:{s:13:"attachment_id";i:23;s:3:"url";s:131:"http://localhost/~katherine.griffiths/wordpress/wp-content/uploads/2014/09/cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg";s:13:"thumbnail_url";s:131:"http://localhost/~katherine.griffiths/wordpress/wp-content/uploads/2014/09/cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg";s:6:"height";i:1131;s:5:"width";i:1500;}s:16:"background_image";s:0:"";s:17:"background_repeat";s:6:"repeat";s:21:"background_position_x";s:4:"left";s:21:"background_attachment";s:6:"scroll";s:18:"origin_font_family";s:10:"Droid Sans";s:16:"origin_font_size";s:2:"16";s:17:"origin_link_color";s:7:"#dd5424";s:17:"origin_custom_css";s:0:"";}', 'yes'),
(115, 'theme_switched', '', 'yes'),
(116, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(117, 'widget_hybrid-archives', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(118, 'widget_hybrid-authors', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(119, 'widget_hybrid-calendar', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(120, 'widget_hybrid-categories', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(121, 'widget_hybrid-nav-menu', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(122, 'widget_hybrid-pages', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(123, 'widget_hybrid-search', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(124, 'widget_hybrid-tags', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(125, 'origin_theme_settings', 'a:1:{s:13:"footer_insert";s:121:"<p class="copyright">Copyright © [the-year] [site-link]</p>\n\n<p class="credit">Powered by [wp-link] and [theme-link]</p>";}', 'yes'),
(126, 'recently_activated', 'a:0:{}', 'yes'),
(127, 'tribe_events_calendar_options', 'a:23:{s:27:"recurring_events_are_hidden";s:6:"hidden";s:19:"tribeEventsTemplate";s:0:"";s:21:"tribeEventsBeforeHTML";s:0:"";s:20:"tribeEventsAfterHTML";s:0:"";s:21:"previous_ecp_versions";a:1:{i:0;s:1:"0";}s:18:"latest_ecp_version";s:3:"3.7";s:14:"welcome_notice";b:1;s:11:"donate-link";b:1;s:12:"postsPerPage";s:2:"10";s:17:"liveFiltersUpdate";b:1;s:12:"showComments";b:0;s:20:"showEventsInMainLoop";b:0;s:10:"eventsSlug";s:6:"events";s:15:"singleEventSlug";s:5:"event";s:14:"multiDayCutoff";s:5:"00:00";s:21:"defaultCurrencySymbol";s:1:"$";s:23:"reverseCurrencyPosition";b:0;s:15:"embedGoogleMaps";b:1;s:19:"embedGoogleMapsZoom";s:2:"10";s:11:"debugEvents";b:0;s:10:"viewOption";s:8:"upcoming";s:13:"earliest_date";s:19:"2014-09-23 06:00:00";s:11:"latest_date";s:19:"2015-11-23 17:00:00";}', 'yes'),
(128, 'tribe_events_db_version', '3.0.0', 'yes'),
(130, 'tribe_events_suite_versions', 'a:1:{s:19:"TribeEventsCalendar";s:3:"3.7";}', 'yes'),
(141, 'rewrite_rules', 'a:159:{s:36:"event/([^/]+)/(\\d{4}-\\d{2}-\\d{2})/?$";s:56:"index.php?tribe_events=$matches[1]&eventDate=$matches[2]";s:41:"event/([^/]+)/(\\d{4}-\\d{2}-\\d{2})/ical/?$";s:63:"index.php?ical=1&tribe_events=$matches[1]&eventDate=$matches[2]";s:20:"event/([^/]+)/all/?$";s:74:"index.php?post_type=tribe_events&tribe_events=$matches[1]&eventDisplay=all";s:17:"events/page/(\\d+)";s:72:"index.php?post_type=tribe_events&eventDisplay=upcoming&paged=$matches[1]";s:11:"events/ical";s:39:"index.php?post_type=tribe_events&ical=1";s:34:"events/(feed|rdf|rss|rss2|atom)/?$";s:71:"index.php?post_type=tribe_events&eventDisplay=upcoming&feed=$matches[1]";s:12:"events/month";s:51:"index.php?post_type=tribe_events&eventDisplay=month";s:26:"events/upcoming/page/(\\d+)";s:72:"index.php?post_type=tribe_events&eventDisplay=upcoming&paged=$matches[1]";s:15:"events/upcoming";s:54:"index.php?post_type=tribe_events&eventDisplay=upcoming";s:22:"events/past/page/(\\d+)";s:68:"index.php?post_type=tribe_events&eventDisplay=past&paged=$matches[1]";s:11:"events/past";s:50:"index.php?post_type=tribe_events&eventDisplay=past";s:12:"events/today";s:49:"index.php?post_type=tribe_events&eventDisplay=day";s:21:"events/(\\d{4}-\\d{2})$";s:73:"index.php?post_type=tribe_events&eventDisplay=month&eventDate=$matches[1]";s:29:"events/(\\d{4}-\\d{2}-\\d{2})/?$";s:71:"index.php?post_type=tribe_events&eventDisplay=day&eventDate=$matches[1]";s:14:"events/feed/?$";s:64:"index.php?post_type=tribe_eventseventDisplay=upcoming&&feed=rss2";s:9:"events/?$";s:53:"index.php?post_type=tribe_events&eventDisplay=default";s:21:"event/([^/]+)/ical/?$";s:56:"index.php?post_type=tribe_events&name=$matches[1]&ical=1";s:35:"events//(\\d{4}-\\d{2}-\\d{2})/ical/?$";s:78:"index.php?post_type=tribe_events&eventDisplay=day&eventDate=$matches[1]&ical=1";s:49:"(.*)events/category/(?:[^/]+/)*([^/]+)/page/(\\d+)";s:101:"index.php?post_type=tribe_events&eventDisplay=upcoming&tribe_events_cat=$matches[2]&paged=$matches[3]";s:44:"(.*)events/category/(?:[^/]+/)*([^/]+)/month";s:80:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=month";s:58:"(.*)events/category/(?:[^/]+/)*([^/]+)/upcoming/page/(\\d+)";s:101:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=upcoming&paged=$matches[3]";s:47:"(.*)events/category/(?:[^/]+/)*([^/]+)/upcoming";s:83:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=upcoming";s:54:"(.*)events/category/(?:[^/]+/)*([^/]+)/past/page/(\\d+)";s:97:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=past&paged=$matches[3]";s:43:"(.*)events/category/(?:[^/]+/)*([^/]+)/past";s:79:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=past";s:44:"(.*)events/category/(?:[^/]+/)*([^/]+)/today";s:78:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=day";s:65:"(.*)events/category/(?:[^/]+/)*([^/]+)/day/(\\d{4}-\\d{2}-\\d{2})/?$";s:100:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=day&eventDate=$matches[3]";s:53:"(.*)events/category/(?:[^/]+/)*([^/]+)/(\\d{4}-\\d{2})$";s:102:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=month&eventDate=$matches[3]";s:59:"(.*)events/category/(?:[^/]+/)*([^/]+)/(\\d{4}-\\d{2}-\\d{2})$";s:100:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=day&eventDate=$matches[3]";s:46:"(.*)events/category/(?:[^/]+/)*([^/]+)/feed/?$";s:93:"index.php?tribe_events_cat=$matches[2]&eventDisplay=upcoming&post_type=tribe_events&feed=rss2";s:46:"(.*)events/category/(?:[^/]+/)*([^/]+)/ical/?$";s:90:"index.php?post_type=tribe_events&eventDisplay=upcoming&tribe_events_cat=$matches[2]&ical=1";s:71:"(.*)events/category/(?:[^/]+/)*([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:78:"index.php?post_type=tribe_events&tribe_events_cat=$matches[2]&feed=$matches[3]";s:41:"(.*)events/category/(?:[^/]+/)*([^/]+)/?$";s:80:"index.php?tribe_events_cat=$matches[2]&post_type=tribe_events&eventDisplay=month";s:33:"(.*)events/tag/([^/]+)/page/(\\d+)";s:88:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=upcoming&paged=$matches[3]";s:28:"(.*)events/tag/([^/]+)/month";s:67:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=month";s:42:"(.*)events/tag/([^/]+)/upcoming/page/(\\d+)";s:88:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=upcoming&paged=$matches[3]";s:31:"(.*)events/tag/([^/]+)/upcoming";s:70:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=upcoming";s:38:"(.*)events/tag/([^/]+)/past/page/(\\d+)";s:84:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=past&paged=$matches[3]";s:27:"(.*)events/tag/([^/]+)/past";s:66:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=past";s:28:"(.*)events/tag/([^/]+)/today";s:65:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=day";s:37:"(.*)events/tag/([^/]+)/(\\d{4}-\\d{2})$";s:89:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=month&eventDate=$matches[3]";s:49:"(.*)events/tag/([^/]+)/day/(\\d{4}-\\d{2}-\\d{2})/?$";s:87:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=day&eventDate=$matches[3]";s:43:"(.*)events/tag/([^/]+)/(\\d{4}-\\d{2}-\\d{2})$";s:87:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=day&eventDate=$matches[3]";s:30:"(.*)events/tag/([^/]+)/feed/?$";s:80:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=upcoming&feed=rss2";s:30:"(.*)events/tag/([^/]+)/ical/?$";s:77:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=upcoming&ical=1";s:55:"(.*)events/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:65:"index.php?tag=$matches[2]&post_type=tribe_events&feed=$matches[3]";s:25:"(.*)events/tag/([^/]+)/?$";s:70:"index.php?tag=$matches[2]&post_type=tribe_events&eventDisplay=upcoming";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:33:"event/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"event/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"event/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"event/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"event/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:26:"event/([^/]+)/trackback/?$";s:39:"index.php?tribe_events=$matches[1]&tb=1";s:34:"event/([^/]+)/page/?([0-9]{1,})/?$";s:52:"index.php?tribe_events=$matches[1]&paged=$matches[2]";s:41:"event/([^/]+)/comment-page-([0-9]{1,})/?$";s:52:"index.php?tribe_events=$matches[1]&cpage=$matches[2]";s:26:"event/([^/]+)(/[0-9]+)?/?$";s:51:"index.php?tribe_events=$matches[1]&page=$matches[2]";s:22:"event/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:32:"event/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:52:"event/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"event/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"event/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"venue/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"venue/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"venue/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"venue/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"venue/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:26:"venue/([^/]+)/trackback/?$";s:38:"index.php?tribe_venue=$matches[1]&tb=1";s:34:"venue/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?tribe_venue=$matches[1]&paged=$matches[2]";s:41:"venue/([^/]+)/comment-page-([0-9]{1,})/?$";s:51:"index.php?tribe_venue=$matches[1]&cpage=$matches[2]";s:26:"venue/([^/]+)(/[0-9]+)?/?$";s:50:"index.php?tribe_venue=$matches[1]&page=$matches[2]";s:22:"venue/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:32:"venue/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:52:"venue/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"venue/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:47:"venue/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:37:"organizer/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"organizer/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"organizer/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"organizer/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"organizer/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"organizer/([^/]+)/trackback/?$";s:42:"index.php?tribe_organizer=$matches[1]&tb=1";s:38:"organizer/([^/]+)/page/?([0-9]{1,})/?$";s:55:"index.php?tribe_organizer=$matches[1]&paged=$matches[2]";s:45:"organizer/([^/]+)/comment-page-([0-9]{1,})/?$";s:55:"index.php?tribe_organizer=$matches[1]&cpage=$matches[2]";s:30:"organizer/([^/]+)(/[0-9]+)?/?$";s:54:"index.php?tribe_organizer=$matches[1]&page=$matches[2]";s:26:"organizer/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:36:"organizer/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:56:"organizer/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"organizer/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"organizer/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:54:"events/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:55:"index.php?tribe_events_cat=$matches[1]&feed=$matches[2]";s:49:"events/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:55:"index.php?tribe_events_cat=$matches[1]&feed=$matches[2]";s:42:"events/category/(.+?)/page/?([0-9]{1,})/?$";s:56:"index.php?tribe_events_cat=$matches[1]&paged=$matches[2]";s:24:"events/category/(.+?)/?$";s:38:"index.php?tribe_events_cat=$matches[1]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)(/[0-9]+)?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";}', 'yes'),
(142, 'tribe_last_save_post', '1411435625', 'yes'),
(144, 'tribe_events_cat_children', 'a:0:{}', 'yes'),
(152, '_transient_timeout_dash_4077549d03da2e451c8b5f002294ff51', '1411476183', 'no'),
(153, '_transient_dash_4077549d03da2e451c8b5f002294ff51', '<div class="rss-widget"><p><strong>RSS Error</strong>: WP HTTP Error: Failed connect to wordpress.org:80; Operation now in progress</p></div><div class="rss-widget"><p><strong>RSS Error</strong>: WP HTTP Error: Failed connect to planet.wordpress.org:80; Operation now in progress</p></div><div class="rss-widget"><ul></ul></div>', 'no'),
(154, 'category_children', 'a:0:{}', 'yes'),
(157, '_site_transient_timeout_theme_roots', '1411436304', 'yes'),
(158, '_site_transient_theme_roots', 'a:5:{s:9:"hemingway";s:7:"/themes";s:6:"origin";s:7:"/themes";s:14:"twentyfourteen";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";s:12:"twentytwelve";s:7:"/themes";}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `onslow_postmeta`
--

CREATE TABLE IF NOT EXISTS `onslow_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=333 ;

--
-- Dumping data for table `onslow_postmeta`
--

INSERT INTO `onslow_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_menu_item_type', 'custom'),
(3, 4, '_menu_item_menu_item_parent', '0'),
(4, 4, '_menu_item_object_id', '4'),
(5, 4, '_menu_item_object', 'custom'),
(6, 4, '_menu_item_target', ''),
(7, 4, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(8, 4, '_menu_item_xfn', ''),
(9, 4, '_menu_item_url', 'http://localhost/~katherine.griffiths/wordpress/'),
(10, 4, '_menu_item_orphaned', '1411349406'),
(11, 5, '_menu_item_type', 'post_type'),
(12, 5, '_menu_item_menu_item_parent', '0'),
(13, 5, '_menu_item_object_id', '2'),
(14, 5, '_menu_item_object', 'page'),
(15, 5, '_menu_item_target', ''),
(16, 5, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(17, 5, '_menu_item_xfn', ''),
(18, 5, '_menu_item_url', ''),
(20, 6, '_menu_item_type', 'custom'),
(21, 6, '_menu_item_menu_item_parent', '0'),
(22, 6, '_menu_item_object_id', '6'),
(23, 6, '_menu_item_object', 'custom'),
(24, 6, '_menu_item_target', ''),
(25, 6, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(26, 6, '_menu_item_xfn', ''),
(27, 6, '_menu_item_url', 'http://localhost/~katherine.griffiths/wordpress/'),
(29, 7, '_menu_item_type', 'post_type'),
(30, 7, '_menu_item_menu_item_parent', '0'),
(31, 7, '_menu_item_object_id', '2'),
(32, 7, '_menu_item_object', 'page'),
(33, 7, '_menu_item_target', ''),
(34, 7, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(35, 7, '_menu_item_xfn', ''),
(36, 7, '_menu_item_url', ''),
(38, 8, '_menu_item_type', 'post_type'),
(39, 8, '_menu_item_menu_item_parent', '0'),
(40, 8, '_menu_item_object_id', '2'),
(41, 8, '_menu_item_object', 'page'),
(42, 8, '_menu_item_target', ''),
(43, 8, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(44, 8, '_menu_item_xfn', ''),
(45, 8, '_menu_item_url', ''),
(47, 9, '_menu_item_type', 'post_type'),
(48, 9, '_menu_item_menu_item_parent', '0'),
(49, 9, '_menu_item_object_id', '2'),
(50, 9, '_menu_item_object', 'page'),
(51, 9, '_menu_item_target', ''),
(52, 9, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(53, 9, '_menu_item_xfn', ''),
(54, 9, '_menu_item_url', ''),
(56, 10, '_menu_item_type', 'post_type'),
(57, 10, '_menu_item_menu_item_parent', '0'),
(58, 10, '_menu_item_object_id', '2'),
(59, 10, '_menu_item_object', 'page'),
(60, 10, '_menu_item_target', ''),
(61, 10, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(62, 10, '_menu_item_xfn', ''),
(63, 10, '_menu_item_url', ''),
(65, 11, '_menu_item_type', 'post_type'),
(66, 11, '_menu_item_menu_item_parent', '5'),
(67, 11, '_menu_item_object_id', '2'),
(68, 11, '_menu_item_object', 'page'),
(69, 11, '_menu_item_target', ''),
(70, 11, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(71, 11, '_menu_item_xfn', ''),
(72, 11, '_menu_item_url', ''),
(74, 12, '_menu_item_type', 'post_type'),
(75, 12, '_menu_item_menu_item_parent', '5'),
(76, 12, '_menu_item_object_id', '2'),
(77, 12, '_menu_item_object', 'page'),
(78, 12, '_menu_item_target', ''),
(79, 12, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(80, 12, '_menu_item_xfn', ''),
(81, 12, '_menu_item_url', ''),
(83, 13, '_menu_item_type', 'post_type'),
(84, 13, '_menu_item_menu_item_parent', '5'),
(85, 13, '_menu_item_object_id', '2'),
(86, 13, '_menu_item_object', 'page'),
(87, 13, '_menu_item_target', ''),
(88, 13, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(89, 13, '_menu_item_xfn', ''),
(90, 13, '_menu_item_url', ''),
(92, 14, '_menu_item_type', 'post_type'),
(93, 14, '_menu_item_menu_item_parent', '10'),
(94, 14, '_menu_item_object_id', '2'),
(95, 14, '_menu_item_object', 'page'),
(96, 14, '_menu_item_target', ''),
(97, 14, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(98, 14, '_menu_item_xfn', ''),
(99, 14, '_menu_item_url', ''),
(101, 15, '_menu_item_type', 'post_type'),
(102, 15, '_menu_item_menu_item_parent', '10'),
(103, 15, '_menu_item_object_id', '2'),
(104, 15, '_menu_item_object', 'page'),
(105, 15, '_menu_item_target', ''),
(106, 15, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(107, 15, '_menu_item_xfn', ''),
(108, 15, '_menu_item_url', ''),
(110, 16, '_menu_item_type', 'post_type'),
(111, 16, '_menu_item_menu_item_parent', '10'),
(112, 16, '_menu_item_object_id', '2'),
(113, 16, '_menu_item_object', 'page'),
(114, 16, '_menu_item_target', ''),
(115, 16, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(116, 16, '_menu_item_xfn', ''),
(117, 16, '_menu_item_url', ''),
(119, 17, '_menu_item_type', 'post_type'),
(120, 17, '_menu_item_menu_item_parent', '10'),
(121, 17, '_menu_item_object_id', '2'),
(122, 17, '_menu_item_object', 'page'),
(123, 17, '_menu_item_target', ''),
(124, 17, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(125, 17, '_menu_item_xfn', ''),
(126, 17, '_menu_item_url', ''),
(128, 18, '_menu_item_type', 'post_type'),
(129, 18, '_menu_item_menu_item_parent', '7'),
(130, 18, '_menu_item_object_id', '2'),
(131, 18, '_menu_item_object', 'page'),
(132, 18, '_menu_item_target', ''),
(133, 18, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(134, 18, '_menu_item_xfn', ''),
(135, 18, '_menu_item_url', ''),
(137, 19, '_menu_item_type', 'post_type'),
(138, 19, '_menu_item_menu_item_parent', '7'),
(139, 19, '_menu_item_object_id', '2'),
(140, 19, '_menu_item_object', 'page'),
(141, 19, '_menu_item_target', ''),
(142, 19, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(143, 19, '_menu_item_xfn', ''),
(144, 19, '_menu_item_url', ''),
(146, 20, '_menu_item_type', 'post_type'),
(147, 20, '_menu_item_menu_item_parent', '10'),
(148, 20, '_menu_item_object_id', '2'),
(149, 20, '_menu_item_object', 'page'),
(150, 20, '_menu_item_target', ''),
(151, 20, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(152, 20, '_menu_item_xfn', ''),
(153, 20, '_menu_item_url', ''),
(155, 21, '_menu_item_type', 'post_type'),
(156, 21, '_menu_item_menu_item_parent', '10'),
(157, 21, '_menu_item_object_id', '2'),
(158, 21, '_menu_item_object', 'page'),
(159, 21, '_menu_item_target', ''),
(160, 21, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(161, 21, '_menu_item_xfn', ''),
(162, 21, '_menu_item_url', ''),
(164, 22, '_wp_attached_file', '2014/09/Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg'),
(165, 22, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2362;s:6:"height";i:2362;s:4:"file";s:56:"2014/09/Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:56:"Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:56:"Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:58:"Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm-1024x1024.jpg";s:5:"width";i:1024;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:16:"single-thumbnail";a:4:{s:4:"file";s:56:"Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm-636x310.jpg";s:5:"width";i:636;s:6:"height";i:310;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:1;}}'),
(166, 22, '_wp_attachment_image_alt', 'Onslow College Rowing'),
(167, 23, '_wp_attached_file', '2014/09/cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg'),
(168, 23, '_wp_attachment_context', 'custom-header'),
(169, 23, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1500;s:6:"height";i:1131;s:4:"file";s:64:"2014/09/cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:64:"cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:64:"cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm-300x226.jpg";s:5:"width";i:300;s:6:"height";i:226;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:65:"cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm-1024x772.jpg";s:5:"width";i:1024;s:6:"height";i:772;s:9:"mime-type";s:10:"image/jpeg";}s:16:"single-thumbnail";a:4:{s:4:"file";s:64:"cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm-636x310.jpg";s:5:"width";i:636;s:6:"height";i:310;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(170, 23, '_wp_attachment_custom_header_last_used_origin', '1411353433'),
(171, 23, '_wp_attachment_is_custom_header', 'origin'),
(172, 24, '_EventAuditTrail', 'a:3:{i:0;a:2:{i:0;s:15:"events-calendar";i:1;i:1411355956;}i:1;a:2:{i:0;s:15:"events-calendar";i:1;i:1411355966;}i:2;a:2:{i:0;s:15:"events-calendar";i:1;i:1411356217;}}'),
(173, 24, '_EventOrigin', 'events-calendar'),
(174, 24, '_edit_last', '1'),
(175, 24, '_edit_lock', '1411356217:1'),
(176, 25, '_VenueAuditTrail', 'a:2:{i:0;a:2:{i:0;s:15:"events-calendar";i:1;i:1411356216;}i:1;a:2:{i:0;s:15:"events-calendar";i:1;i:1411432627;}}'),
(177, 25, '_VenueOrigin', 'events-calendar'),
(178, 25, '_EventShowMapLink', ''),
(179, 25, '_EventShowMap', ''),
(180, 25, '_VenueVenue', 'Lake Karapiro'),
(181, 25, '_VenueAddress', 'The Lake'),
(182, 25, '_VenueCity', 'Karapiro'),
(183, 25, '_VenueCountry', 'New Zealand'),
(184, 25, '_VenueProvince', 'Waikato'),
(185, 25, '_VenueState', ''),
(186, 25, '_VenueZip', ''),
(187, 25, '_VenuePhone', '234567'),
(188, 25, '_VenueURL', 'www.maadi.co.nz'),
(189, 25, '_VenueStateProvince', 'Waikato'),
(190, 24, '_preview_venue_id', '25'),
(191, 26, '_OrganizerAuditTrail', 'a:1:{i:0;a:2:{i:0;s:15:"events-calendar";i:1;i:1411356216;}}'),
(192, 26, '_OrganizerOrigin', 'events-calendar'),
(193, 26, '_OrganizerOrganizer', 'NZSSRA'),
(194, 26, '_OrganizerPhone', ''),
(195, 26, '_OrganizerWebsite', 'www.maadi.co.nz'),
(196, 26, '_OrganizerEmail', ''),
(197, 24, '_preview_organizer_id', '26'),
(198, 24, '_EventShowMapLink', '1'),
(199, 24, '_EventShowMap', '1'),
(200, 24, '_EventAllDay', 'yes'),
(201, 24, '_EventStartDate', '2015-03-23 00:00:00'),
(202, 24, '_EventEndDate', '2015-03-28 23:59:59'),
(203, 24, '_EventDuration', '518399'),
(204, 24, '_EventVenueID', '25'),
(205, 24, '_EventCurrencySymbol', '$'),
(206, 24, '_EventCurrencyPosition', 'prefix'),
(207, 24, '_EventCost', ''),
(208, 24, '_EventURL', 'www.maadi.co.nz'),
(209, 24, '_EventOrganizerID', '26'),
(210, 27, '_EventAuditTrail', 'a:1:{i:0;a:2:{i:0;s:15:"events-calendar";i:1;i:1411431000;}}'),
(211, 27, '_EventOrigin', 'events-calendar'),
(212, 28, '_EventAuditTrail', 'a:3:{i:0;a:2:{i:0;s:15:"events-calendar";i:1;i:1411432254;}i:1;a:2:{i:0;s:15:"events-calendar";i:1;i:1411432281;}i:2;a:2:{i:0;s:15:"events-calendar";i:1;i:1411432401;}}'),
(213, 28, '_EventOrigin', 'events-calendar'),
(214, 28, '_edit_last', '1'),
(215, 28, '_edit_lock', '1411434728:1'),
(216, 28, 'dfgfdgfgsfdf ddsfdfsdfjhjkhknjhjsfb', 'fdgvgfnjiopjhcxcvbnm,jhgjkshgjlfhgjlfhkldjflkdsjhgklhglkfj9489e6tjgklsd'),
(217, 28, '_EventShowMapLink', '1'),
(218, 28, '_EventShowMap', '1'),
(219, 29, '_OrganizerOrigin', 'events-calendar'),
(220, 29, '_OrganizerOrganizerID', '0'),
(221, 29, '_OrganizerOrganizer', 'Onslow College Team 5'),
(222, 29, '_OrganizerPhone', '234567890'),
(223, 29, '_OrganizerWebsite', ''),
(224, 29, '_OrganizerEmail', ''),
(225, 30, '_VenueOrigin', 'events-calendar'),
(226, 30, '_EventShowMapLink', '1'),
(227, 30, '_EventShowMap', '1'),
(228, 30, '_VenueVenueID', '0'),
(229, 30, '_VenueVenue', ''),
(230, 30, '_VenueAddress', ''),
(231, 30, '_VenueCity', 'Wellington'),
(232, 30, '_VenueCountry', 'New Zealand'),
(233, 30, '_VenueProvince', ''),
(234, 30, '_VenueState', ''),
(235, 30, '_VenueZip', ''),
(236, 30, '_VenuePhone', ''),
(237, 30, '_VenueURL', ''),
(238, 30, '_VenueStateProvince', ''),
(239, 28, '_EventStartDate', '2014-09-23 06:00:00'),
(240, 28, '_EventEndDate', '2014-09-23 08:00:00'),
(241, 28, '_EventDuration', '7200'),
(242, 28, '_EventVenueID', '30'),
(243, 28, '_EventCurrencySymbol', '$'),
(244, 28, '_EventCurrencyPosition', 'prefix'),
(245, 28, '_EventCost', '25'),
(246, 28, '_EventURL', ''),
(247, 28, '_EventOrganizerID', '29'),
(248, 25, '_edit_lock', '1411432486:1'),
(249, 25, '_edit_last', '1'),
(250, 25, '_VenueShowMap', 'true'),
(251, 25, '_VenueShowMapLink', 'true'),
(252, 31, '_menu_item_type', 'post_type'),
(253, 31, '_menu_item_menu_item_parent', '8'),
(254, 31, '_menu_item_object_id', '2'),
(255, 31, '_menu_item_object', 'page'),
(256, 31, '_menu_item_target', ''),
(257, 31, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(258, 31, '_menu_item_xfn', ''),
(259, 31, '_menu_item_url', ''),
(261, 32, '_menu_item_type', 'post_type'),
(262, 32, '_menu_item_menu_item_parent', '8'),
(263, 32, '_menu_item_object_id', '2'),
(264, 32, '_menu_item_object', 'page'),
(265, 32, '_menu_item_target', ''),
(266, 32, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(267, 32, '_menu_item_xfn', ''),
(268, 32, '_menu_item_url', ''),
(270, 33, '_menu_item_type', 'post_type'),
(271, 33, '_menu_item_menu_item_parent', '8'),
(272, 33, '_menu_item_object_id', '2'),
(273, 33, '_menu_item_object', 'page'),
(274, 33, '_menu_item_target', ''),
(275, 33, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(276, 33, '_menu_item_xfn', ''),
(277, 33, '_menu_item_url', ''),
(279, 1, '_edit_lock', '1411432929:1'),
(280, 1, '_edit_last', '1'),
(282, 1, '_wp_old_slug', 'hello-world'),
(283, 36, '_EventAuditTrail', 'a:2:{i:0;a:2:{i:0;s:15:"events-calendar";i:1;i:1411435133;}i:1;a:2:{i:0;s:15:"events-calendar";i:1;i:1411435139;}}'),
(284, 36, '_EventOrigin', 'events-calendar'),
(285, 36, '_edit_last', '1'),
(286, 36, '_edit_lock', '1411435010:1'),
(287, 36, '_EventShowMapLink', '1'),
(288, 36, '_EventShowMap', '1'),
(289, 36, '_EventStartDate', '2014-11-06 08:00:00'),
(290, 36, '_EventEndDate', '2014-11-06 17:00:00'),
(291, 36, '_EventDuration', '32400'),
(292, 36, '_EventVenueID', '0'),
(293, 36, '_EventCurrencySymbol', '$'),
(294, 36, '_EventCurrencyPosition', 'prefix'),
(295, 36, '_EventCost', ''),
(296, 36, '_EventURL', ''),
(297, 36, '_EventOrganizerID', '0'),
(298, 37, '_EventAuditTrail', 'a:3:{i:0;a:2:{i:0;s:15:"events-calendar";i:1;i:1411435347;}i:1;a:2:{i:0;s:15:"events-calendar";i:1;i:1411435419;}i:2;a:2:{i:0;s:15:"events-calendar";i:1;i:1411435539;}}'),
(299, 37, '_EventOrigin', 'events-calendar'),
(300, 37, '_edit_last', '1'),
(301, 37, '_edit_lock', '1411435488:1'),
(302, 37, '_EventShowMapLink', '1'),
(303, 37, '_EventShowMap', '1'),
(304, 38, '_OrganizerOrigin', 'events-calendar'),
(305, 38, '_OrganizerOrganizerID', '0'),
(306, 38, '_OrganizerOrganizer', 'ertyui nfghjkl'),
(307, 38, '_OrganizerPhone', '567890'),
(308, 38, '_OrganizerWebsite', ''),
(309, 38, '_OrganizerEmail', ''),
(310, 39, '_VenueOrigin', 'events-calendar'),
(311, 39, '_EventShowMapLink', '1'),
(312, 39, '_EventShowMap', '1'),
(313, 39, '_VenueVenueID', '0'),
(314, 39, '_VenueVenue', 'Wellington Harbour'),
(315, 39, '_VenueAddress', '123 blah st'),
(316, 39, '_VenueCity', 'wellington'),
(317, 39, '_VenueCountry', 'New Zealand'),
(318, 39, '_VenueProvince', ''),
(319, 39, '_VenueState', ''),
(320, 39, '_VenueZip', ''),
(321, 39, '_VenuePhone', ''),
(322, 39, '_VenueURL', ''),
(323, 39, '_VenueStateProvince', ''),
(324, 37, '_EventStartDate', '2015-11-23 08:00:00'),
(325, 37, '_EventEndDate', '2015-11-23 17:00:00'),
(326, 37, '_EventDuration', '32400'),
(327, 37, '_EventVenueID', '39'),
(328, 37, '_EventCurrencySymbol', '$'),
(329, 37, '_EventCurrencyPosition', 'prefix'),
(330, 37, '_EventCost', ''),
(331, 37, '_EventURL', ''),
(332, 37, '_EventOrganizerID', '38');

-- --------------------------------------------------------

--
-- Table structure for table `onslow_posts`
--

CREATE TABLE IF NOT EXISTS `onslow_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `onslow_posts`
--

INSERT INTO `onslow_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2014-09-22 00:26:10', '2014-09-22 00:26:10', '<ul>\r\n	<li>Advanced Canonical URLs</li>\r\n	<li>Fine tune Page Navigational Links</li>\r\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\r\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\r\n	<li>Nonce Security built into All in One SEO Pack</li>\r\n	<li>Support for CMS-style WordPress installations</li>\r\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\r\n	<li>Generates <strong>META tags automatically</strong></li>\r\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\r\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\r\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\r\n	<li>You can override any title and set any META description and any META keywords you want.</li>\r\n</ul>', 'Karapiro Event 27/09/2014', '', 'publish', 'open', 'open', '', 'karapiroevent', '', '', '2014-09-23 00:44:31', '2014-09-23 00:44:31', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=1', 0, 'post', '', 1),
(2, 1, '2014-09-22 00:26:10', '2014-09-22 00:26:10', 'This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my blog. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href="http://localhost/~katherine.griffiths/wordpress/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'open', 'open', '', 'sample-page', '', '', '2014-09-22 00:26:10', '2014-09-22 00:26:10', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?page_id=2', 0, 'page', '', 0),
(3, 1, '2014-09-22 00:26:37', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2014-09-22 00:26:37', '0000-00-00 00:00:00', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=3', 0, 'post', '', 0),
(4, 1, '2014-09-22 01:30:06', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'open', 'open', '', '', '', '', '2014-09-22 01:30:06', '0000-00-00 00:00:00', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=4', 1, 'nav_menu_item', '', 0),
(5, 1, '2014-09-22 01:34:34', '2014-09-22 01:34:34', '', 'Regatta Info', 'who am i?', 'publish', 'open', 'open', '', 'regatta-info', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=5', 2, 'nav_menu_item', '', 0),
(6, 1, '2014-09-22 01:34:34', '2014-09-22 01:34:34', '', 'Home', '', 'publish', 'open', 'open', '', 'home', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=6', 1, 'nav_menu_item', '', 0),
(7, 1, '2014-09-22 01:34:34', '2014-09-22 01:34:34', '', 'Training', 'But, wait! Who am i?', 'publish', 'open', 'open', '', 'training', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=7', 6, 'nav_menu_item', '', 0),
(8, 1, '2014-09-22 01:34:34', '2014-09-22 01:34:34', '', 'Squad News', 'boop', 'publish', 'open', 'open', '', 'squad-news', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=8', 9, 'nav_menu_item', '', 0),
(9, 1, '2014-09-22 01:34:34', '2014-09-22 01:34:34', '', 'Fundraising', 'We put the fun in it', 'publish', 'open', 'open', '', 'fundraising', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=9', 13, 'nav_menu_item', '', 0),
(10, 1, '2014-09-22 01:34:34', '2014-09-22 01:34:34', '', 'About Us', 'beepbeep', 'publish', 'open', 'open', '', 'about-us', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=10', 14, 'nav_menu_item', '', 0),
(11, 1, '2014-09-22 01:37:58', '2014-09-22 01:37:58', '', 'Maps', '', 'publish', 'open', 'open', '', '11', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=11', 3, 'nav_menu_item', '', 0),
(12, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Results', '', 'publish', 'open', 'open', '', 'results', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=12', 4, 'nav_menu_item', '', 0),
(13, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Photo Album', '', 'publish', 'open', 'open', '', 'photo-album', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=13', 5, 'nav_menu_item', '', 0),
(14, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Contact Us', '', 'publish', 'open', 'open', '', 'contact-us', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=14', 15, 'nav_menu_item', '', 0),
(15, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'How to join Onslow Rowing', '', 'publish', 'open', 'open', '', 'how-to-join-onslow-rowing', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=15', 16, 'nav_menu_item', '', 0),
(16, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Onslow Rowing Club Handbook', '', 'publish', 'open', 'open', '', 'onslow-rowing-club-handbook', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=16', 17, 'nav_menu_item', '', 0),
(17, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Onslow Rowing uniform requirements', '', 'publish', 'open', 'open', '', 'onslow-rowing-uniform-requirements', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=17', 18, 'nav_menu_item', '', 0),
(18, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Nutrition', '', 'publish', 'open', 'open', '', 'nutrition', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=18', 7, 'nav_menu_item', '', 0),
(19, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Event Catering', '', 'publish', 'open', 'open', '', 'event-catering', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=19', 8, 'nav_menu_item', '', 0),
(20, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Sponsors', '', 'publish', 'open', 'open', '', 'sponsors', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=20', 20, 'nav_menu_item', '', 0),
(21, 1, '2014-09-22 01:46:06', '2014-09-22 01:46:06', '', 'Lost and Found', '', 'publish', 'open', 'open', '', 'lost-and-found', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=21', 19, 'nav_menu_item', '', 0),
(22, 1, '2014-09-22 02:35:58', '2014-09-22 02:35:58', '', 'Onslow College Rowing Logo - FINAL - 20cm x 20cm', '', 'inherit', 'open', 'open', '', 'onslow-college-rowing-logo-final-20cm-x-20cm', '', '', '2014-09-22 02:36:17', '2014-09-22 02:36:17', '', 0, 'http://localhost/~katherine.griffiths/wordpress/wp-content/uploads/2014/09/Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg', 0, 'attachment', 'image/jpeg', 0),
(23, 1, '2014-09-22 02:36:43', '2014-09-22 02:36:43', 'http://localhost/~katherine.griffiths/wordpress/wp-content/uploads/2014/09/cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg', 'cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg', '', 'inherit', 'open', 'open', '', 'cropped-onslow-college-rowing-logo-final-20cm-x-20cm-jpg', '', '', '2014-09-22 02:36:43', '2014-09-22 02:36:43', '', 0, 'http://localhost/~katherine.griffiths/wordpress/wp-content/uploads/2014/09/cropped-Onslow-College-Rowing-Logo-FINAL-20cm-x-20cm.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2014-09-22 03:23:36', '0000-00-00 00:00:00', '', 'Maadi Cup 2015', '', 'draft', 'open', 'open', '', '', '', '', '2014-09-22 03:23:36', '2014-09-22 03:23:36', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?post_type=tribe_events&#038;p=24', 0, 'tribe_events', '', 0),
(25, 1, '2014-09-23 00:37:07', '2014-09-23 00:37:07', '<ul>\r\n	<li>]\r\n(<small>G U18 8+</small>) [<a href="http://www.rowit.co.nz/results/mads2014?en=47" target="_blank">results</a>]</li>\r\n	<li><small>Simon Briggs Cup: <a href="http://www.rowit.co.nz/results/mads2014?pid=18815" target="_blank">Natalie Bocock</a>, St Margarets College\r\n(Coxswain of the regatta)</small></li>\r\n	<li><small>Bill Eady Cup: Dan Kelly from Roncalli College and Timaru Girls High School\r\n(RowingNZ''s Junior coach of the year: domestic &amp; international competition)</small></li>\r\n</ul>\r\n<b>A big thank you to Ruataniwha Rowing and their many officials and volunteers who have hosted a massive and superb 2014 Aon Maadi Cup regatta!</b>', 'Lake Karapiro', '', 'publish', 'closed', 'closed', '', 'lake-karapiro', '', '', '2014-09-23 00:37:07', '2014-09-23 00:37:07', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?post_type=tribe_venue&#038;p=25', 0, 'tribe_venue', '', 0),
(26, 1, '2014-09-22 03:23:36', '0000-00-00 00:00:00', '', 'NZSSRA', '', 'draft', 'open', 'open', '', '', '', '', '2014-09-22 03:23:36', '0000-00-00 00:00:00', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?post_type=tribe_organizer&p=26', 0, 'tribe_organizer', '', 0),
(27, 1, '2014-09-23 00:10:00', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2014-09-23 00:10:00', '0000-00-00 00:00:00', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?post_type=tribe_events&p=27', 0, 'tribe_events', '', 0),
(28, 1, '2014-09-23 00:33:50', '2014-09-23 00:33:50', 'ghfgsdfhjkdsgfhjdsgfhjgsdhjfgjskdfhesfgsdhjfgdshjf\r\n\r\n&nbsp;\r\n<ul>\r\n	<li>Advanced Canonical URLs</li>\r\n	<li>Fine tune Page Navigational Links</li>\r\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\r\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\r\n	<li>Nonce Security built into All in One SEO Pack</li>\r\n	<li>Support for CMS-style WordPress installations</li>\r\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\r\n	<li>Generates <strong>META tags automatically</strong></li>\r\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\r\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\r\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\r\n	<li>You can override any title and set any META description and any META keywords you want.</li>\r\n	<li>Compatibility with many other plugins</li>\r\n</ul>\r\n<ul>\r\n	<li>Advanced Canonical URLs</li>\r\n	<li>Fine tune Page Navigational Links</li>\r\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\r\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\r\n	<li>Nonce Security built into All in One SEO Pack</li>\r\n	<li>Support for CMS-style WordPress installations</li>\r\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\r\n	<li>Generates <strong>META tags automatically</strong></li>\r\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\r\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\r\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\r\n	<li>You can override any title and set any META description and any META keywords you want.</li>\r\n	<li>Compatibility with many other plugins</li>\r\n</ul>', 'training that day', 'added information dot com', 'publish', 'open', 'open', '', 'training-that-day', '', '', '2014-09-23 01:14:29', '2014-09-23 01:14:29', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?post_type=tribe_events&#038;p=28', -1, 'tribe_events', '', 0),
(29, 1, '2014-09-23 00:33:50', '2014-09-23 00:33:50', '', 'Onslow College Team 5', '', 'publish', 'open', 'open', '', 'onslow-college-team-5', '', '', '2014-09-23 00:33:50', '2014-09-23 00:33:50', '', 0, 'http://localhost/~katherine.griffiths/wordpress/organizer/onslow-college-team-5/', 0, 'tribe_organizer', '', 0),
(30, 1, '2014-09-23 00:33:50', '2014-09-23 00:33:50', '', 'Unnamed Venue', '', 'publish', 'open', 'open', '', 'unnamed-venue', '', '', '2014-09-23 00:33:50', '2014-09-23 00:33:50', '', 0, 'http://localhost/~katherine.griffiths/wordpress/venue/unnamed-venue/', 0, 'tribe_venue', '', 0),
(31, 1, '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 'Team One', '', 'publish', 'open', 'open', '', 'team-one', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=31', 10, 'nav_menu_item', '', 0),
(32, 1, '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 'Team  Two', '', 'publish', 'open', 'open', '', 'team-two', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=32', 11, 'nav_menu_item', '', 0),
(33, 1, '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 'Team Three', '', 'publish', 'open', 'open', '', 'team-three', '', '', '2014-09-23 00:41:21', '2014-09-23 00:41:21', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?p=33', 12, 'nav_menu_item', '', 0),
(34, 1, '2014-09-23 00:43:39', '2014-09-23 00:43:39', '<ul>\n	<li>Advanced Canonical URLs</li>\n	<li>Fine tune Page Navigational Links</li>\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\n	<li>Nonce Security built into All in One SEO Pack</li>\n	<li>Support for CMS-style WordPress installations</li>\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\n	<li>Generates <strong>META tags automatically</strong></li>\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\n	<li>You can override any title and set any META description and any META keywords you want.</li>\n</ul>', 'Hello world!', '', 'inherit', 'open', 'open', '', '1-autosave-v1', '', '', '2014-09-23 00:43:39', '2014-09-23 00:43:39', '', 1, 'http://localhost/~katherine.griffiths/wordpress/1-autosave-v1/', 0, 'revision', '', 0),
(35, 1, '2014-09-23 00:44:31', '2014-09-23 00:44:31', '<ul>\r\n	<li>Advanced Canonical URLs</li>\r\n	<li>Fine tune Page Navigational Links</li>\r\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\r\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\r\n	<li>Nonce Security built into All in One SEO Pack</li>\r\n	<li>Support for CMS-style WordPress installations</li>\r\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\r\n	<li>Generates <strong>META tags automatically</strong></li>\r\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\r\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\r\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\r\n	<li>You can override any title and set any META description and any META keywords you want.</li>\r\n</ul>', 'Karapiro Event 27/09/2014', '', 'inherit', 'open', 'open', '', '1-revision-v1', '', '', '2014-09-23 00:44:31', '2014-09-23 00:44:31', '', 1, 'http://localhost/~katherine.griffiths/wordpress/1-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2014-09-23 01:19:11', '2014-09-23 01:19:11', '<ul>\r\n	<li>Advanced Canonical URLs</li>\r\n	<li>Fine tune Page Navigational Links</li>\r\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\r\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\r\n	<li>Nonce Security built into All in One SEO Pack</li>\r\n	<li>Support for CMS-style WordPress installations</li>\r\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\r\n	<li>Generates <strong>META tags automatically</strong></li>\r\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\r\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\r\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\r\n	<li>You can override any title and set any META description and any META keywords you want.</li>\r\n	<li>Compatibility with many other plugins\r\n<ul>\r\n	<li>Advanced Canonical URLs</li>\r\n	<li>Fine tune Page Navigational Links</li>\r\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\r\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\r\n	<li>Nonce Security built into All in One SEO Pack</li>\r\n	<li>Support for CMS-style WordPress installations</li>\r\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\r\n	<li>Generates <strong>META tags automatically</strong></li>\r\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\r\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\r\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\r\n	<li>You can override any title and set any META description and any META keywords you want.</li>\r\n	<li>Compatibility with many other plugins\r\n<ul>\r\n	<li>Advanced Canonical URLs</li>\r\n	<li>Fine tune Page Navigational Links</li>\r\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\r\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\r\n	<li>Nonce Security built into All in One SEO Pack</li>\r\n	<li>Support for CMS-style WordPress installations</li>\r\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\r\n	<li>Generates <strong>META tags automatically</strong></li>\r\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\r\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\r\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\r\n	<li>You can override any title and set any META description and any META keywords you want.</li>\r\n	<li>Compatibility with many other plugins\r\n<ul>\r\n	<li>Advanced Canonical URLs</li>\r\n	<li>Fine tune Page Navigational Links</li>\r\n	<li>Built-in API so other plugins/themes can access and extend functionality</li>\r\n	<li>ONLY plugin to provide SEO Integration for WP e-Commerce sites</li>\r\n	<li>Nonce Security built into All in One SEO Pack</li>\r\n	<li>Support for CMS-style WordPress installations</li>\r\n	<li>Automatically optimizes your <strong>titles</strong> for Google and other search engines</li>\r\n	<li>Generates <strong>META tags automatically</strong></li>\r\n	<li>Avoids the typical duplicate content found on WordPress blogs</li>\r\n	<li>For beginners, you don''t even have to look at the options, it works out-of-the-box. Just install.</li>\r\n	<li>For advanced users, you can fine-tune everything to optimize your SEO</li>\r\n	<li>You can override any title and set any META description and any META keywords you want.</li>\r\n	<li>Compatibility with many other plugins</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n</li>\r\n</ul>', 'that other event', '', 'publish', 'open', 'open', '', 'that-other-event', '', '', '2014-09-23 01:19:11', '2014-09-23 01:19:11', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?post_type=tribe_events&#038;p=36', 0, 'tribe_events', '', 0),
(37, 1, '2014-09-23 01:27:05', '2014-09-23 01:27:05', 'Get blah cus blah blah blah\r\n\r\n7372398 \\\r\n\r\nlugsdvkjhfh lkjh\r\n\r\n12`''fojg', 'A day in the life', '', 'publish', 'closed', 'open', '', 'a-day-in-the-life', '', '', '2014-09-23 01:27:05', '2014-09-23 01:27:05', '', 0, 'http://localhost/~katherine.griffiths/wordpress/?post_type=tribe_events&#038;p=37', 0, 'tribe_events', '', 0),
(38, 1, '2014-09-23 01:27:05', '2014-09-23 01:27:05', '', 'ertyui nfghjkl', '', 'publish', 'open', 'open', '', 'ertyui-nfghjkl', '', '', '2014-09-23 01:27:05', '2014-09-23 01:27:05', '', 0, 'http://localhost/~katherine.griffiths/wordpress/organizer/ertyui-nfghjkl/', 0, 'tribe_organizer', '', 0),
(39, 1, '2014-09-23 01:27:05', '2014-09-23 01:27:05', '', 'Wellington Harbour', '', 'publish', 'open', 'open', '', 'wellington-harbour', '', '', '2014-09-23 01:27:05', '2014-09-23 01:27:05', '', 0, 'http://localhost/~katherine.griffiths/wordpress/venue/wellington-harbour/', 0, 'tribe_venue', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `onslow_terms`
--

CREATE TABLE IF NOT EXISTS `onslow_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `onslow_terms`
--

INSERT INTO `onslow_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Primary Menu', 'primary-menu', 0),
(3, 'team 5', 'team-5', 0),
(4, 'Event Day', 'event-day', 0),
(5, 'Karapiro', 'karapiro', 0),
(6, 'Event', 'event', 0);

-- --------------------------------------------------------

--
-- Table structure for table `onslow_term_relationships`
--

CREATE TABLE IF NOT EXISTS `onslow_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `onslow_term_relationships`
--

INSERT INTO `onslow_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 4, 0),
(1, 5, 0),
(1, 6, 0),
(5, 2, 0),
(6, 2, 0),
(7, 2, 0),
(8, 2, 0),
(9, 2, 0),
(10, 2, 0),
(11, 2, 0),
(12, 2, 0),
(13, 2, 0),
(14, 2, 0),
(15, 2, 0),
(16, 2, 0),
(17, 2, 0),
(18, 2, 0),
(19, 2, 0),
(20, 2, 0),
(21, 2, 0),
(28, 3, 0),
(31, 2, 0),
(32, 2, 0),
(33, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `onslow_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `onslow_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `onslow_term_taxonomy`
--

INSERT INTO `onslow_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 20),
(3, 3, 'post_tag', '', 0, 1),
(4, 4, 'category', '', 0, 1),
(5, 5, 'post_tag', '', 0, 1),
(6, 6, 'post_tag', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `onslow_usermeta`
--

CREATE TABLE IF NOT EXISTS `onslow_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `onslow_usermeta`
--

INSERT INTO `onslow_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'onslow_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'onslow_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets'),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'session_tokens', 'a:1:{s:64:"d9a6a8afb6bb026dde1f3eac2fb083a370a9226b7e4276b3ff57abdd48dc1803";i:1412555180;}'),
(15, 1, 'onslow_dashboard_quick_press_last_post_id', '3'),
(16, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(17, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:8:"add-post";i:1;s:12:"add-post_tag";}'),
(18, 1, 'nav_menu_recently_edited', '2'),
(19, 1, 'onslow_user-settings', 'libraryContent=browse'),
(20, 1, 'onslow_user-settings-time', '1411354609'),
(21, 1, 'tribe_setDefaultNavMenuBoxes', '1');

-- --------------------------------------------------------

--
-- Table structure for table `onslow_users`
--

CREATE TABLE IF NOT EXISTS `onslow_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `onslow_users`
--

INSERT INTO `onslow_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BAmFP7TPilmOB.Q/T8FpQYbDJ/KDyE1', 'admin', 'kategriffithsphotography@gmail.com', '', '2014-09-22 00:26:10', '', 0, 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
