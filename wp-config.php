<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'onslow_rowing_club');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^3{0txA0v?1*26cGXtk:}AW@s;!TBA4O@GvaO:`{k<aTE!9SVIe#=/wgGjk?v80 ');
define('SECURE_AUTH_KEY',  '?.HX6^>rZ0_C>Z#Q6xlcs3g*fB*O7`c9]xs6=&q[9#TvQ:8QPMOZor7T]@1ENaPb');
define('LOGGED_IN_KEY',    'U+ NH`98=X8z0&wbk fL=D[OzxMH4W$){;(q:;^TH8No@ NAO-7 _d7|+LJ?A~ez');
define('NONCE_KEY',        ',jimFNCTi=^t_~nc>`ek&R`3r>ZrN)v975Am8=dy^ExHUYi[hB0i,$[J9j_MrIxb');
define('AUTH_SALT',        'v7R(DaeRP{rutV109||=8r=WR`jd<Q!+IccM@ui 8gyqiaY;z|Fd&sE:<](|o|Fl');
define('SECURE_AUTH_SALT', 'v)vi1mtwB6:#WMcuWEH <3Dm5zTcNXI>-7|~zIr&K/!gwv<p^ P27m3sO#mc0+,k');
define('LOGGED_IN_SALT',   'p)<z@W<HxwV87JvUqN*$$,WO5h`L&aa0O}hePX4!Np~Cg6R3M.%Ak#%RK8T_6,<_');
define('NONCE_SALT',       '2B*g.}<K_S/rSZt~!4*O`D|Q|.+E)VWMPJV?A}WSS-dT/7PO/><lO]w^_H.L=ApK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'onslow_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
